<head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- CSRF Token -->
    	<meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Seltzermayberg</title>
<!--
        <link rel="shortcut icon" href="images/common/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/common/favicon.ico" type="image/x-icon">
-->
        <!-- SEO metas -->
<!--        <meta name="description" content="Capital Numbers är ett certifierat, prisbelönt företag i världsklass med över 400 anställda. Låt oss hjälpa dig med ditt nya projekt!">-->
        <!--Google font-->
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">
        <!-- SEO metas -->
        <!-- <meta name="description" content="">
        <meta name="keywords" content=""> -->
        <!--Bootstrap css-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
        <!-- include css-include -->
        <link rel="stylesheet" type="text/css" href="{{{ asset('css/style.css') }}}">

    </head>