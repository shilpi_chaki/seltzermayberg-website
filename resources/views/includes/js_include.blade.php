<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
<script>
  $(document).ready(function() {
		(function($){
			$(document).ready(function(){
				$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
					event.preventDefault(); 
					event.stopPropagation(); 
					$(this).parent().siblings().removeClass('open');
					$(this).parent().toggleClass('open');
				});
			});
		})(jQuery);
//    $("#boxscroll").niceScroll({cursorborder:"",cursorwidth:7,background:"#e5e5e5",cursorcolor:"#41D789",boxzoom:true});



		/***** Home Banner Slider ********/
		$('#home-slider').owlCarousel({
			loop:true,
			nav: false,
			margin:10,
			items:1,
			autoPlay:true,
			autoplayTimeout:1000,
			autoplayHoverPause:true,
			responsive:{
		        0:{
		            dots: false
		        },
		        768:{
		            dots: true
		        }
		    }
		});

		/***** Home Clients Slider ********/
		$('#clients-slider-home').owlCarousel({
			loop:true,
			margin:30,
			items:7,
			nav:false,
			dots: false,
			autoPlay:true,
			autoplayTimeout:1000,
			autoplayHoverPause:true,
			pagination: false,
			responsive:{
		        0:{
		            items:3,
		        },
		        600:{
		            items:7,
		        }
		    }
		});

		/***** Home Clients Slider ********/
		$('#client-reviews').owlCarousel({
			loop:true,
			margin:10,
			items:1,
			nav:true,
			autoPlay:true,
			autoplayTimeout:1000,
			autoplayHoverPause:true,
			pagination: false,
			responsive:{
		        0:{
		            nav:false
		        },
		        800:{
		            nav:true
		        },
		    }
		});


		/***** Our Featured Works Slider*****/
			$('#team-slider').owlCarousel({
			loop:true,
			margin:30,
			items:3,
			navigation:true,
			navigationText: ["<i class='fas fa-chevron-left'></i>","<i class='fas fa-chevron-right'></i>"],
			autoPlay:true,
			autoplayTimeout:1000,
			autoplayHoverPause:true,
			pagination: false,
			responsiveClass:true,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:2,
		            nav:false,
		        },
		        1000:{
		            items:3,
		            nav:true,
		            loop:false
		        }
		    }
		});


		/***** Our Featured Works Slider*****/
			$('#case-studies').owlCarousel({
			loop:true,
			margin:30,
			items:1,
			navigation:true,
			navigationText: ["<i class='fas fa-chevron-left'></i>","<i class='fas fa-chevron-right'></i>"],
			autoPlay:true,
			autoplayTimeout:1000,
			autoplayHoverPause:true,
			pagination: false,
			responsiveClass:true,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1,
		            nav:false,
		        },
		        1000:{
		            items:1,
		            nav:true,
		            loop:false
		        }
		    }
		});

		});
</script>

<script type="text/javascript">
	function play(){document.getElementById('vidwrap').innerHTML = '<iframe width="100%" height="513" src="http://www.youtube.com/embed/QH2-TGUlwu4?controls=0" frameborder="0"></iframe>';}
</script>
