<footer>
				<div class="">
					<div class="container">
						<div class="row">
							<div class="col-md-4">
								<div class="footer-blocks">
									<img src="image/footer-logo.png" class="img-responsive" alt="" />
									<h3 class="footer-heading">About US</h3>
									<p>Our founding attorney, David S. Seltzer, and the entire legal team is committed to litigating cases on behalf of our clients. We focus all of our firm's efforts on adequately protecting the rights of our clients.</p>
									<div class="follow-social">
										<ul>
											<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
											<li><a href="#"><i class="fab fa-twitter"></i></a></li>
											<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
											<li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
											<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							
							<div class="col-md-5">
								<div class="footer-blocks">
									<h3 class="footer-heading font-size-24 margin-bottom-20">Newsletter</h3>
									<p>Li lingues differe solmen  grammatic proLi Europan lingues es membres del sam familie. Lor separat existentie es un myth.</p>
									<input type="text" class="form-control" placeholder="Enter your email here"/>
									<div class="newsletter-icon">
										<i class="fab fa-telegram-plane"></i>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="footer-blocks contact">
									<h3 class="footer-heading">Contact Us</h3>
											<div class="contact-footer">
												<h4>Our Miami Address</h4>
												<p>10750 NW 6th CT Miami, Fl 331684</p>
												<h4>Phone</h4>
												<p><a href="tel:3053301336">(305) 330-1336</a></p>
												<h4>Website</h4>
												<a href="mailto:">www.seltzermayberg.com</a></p>
											</div>
									</div>
								</div>
						</div>
					</div>
				</div>

				<div class="copyright-section text-center">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<p>Copyright © 2020 Seltzer Mayberg, All rights reserved</p>
							</div>
						</div>
					</div>
				</div>
		
				

			</footer>


        
				


</body>

</html>