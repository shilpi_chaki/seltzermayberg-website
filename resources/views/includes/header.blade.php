<body>

    		<!--Header starts here-->
        <header>
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="company-info">
									<p>Call Now: 305-444-1565 (AVAILABLE 24/7 / HABLAMOS ESPAÑOL)</p>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="follow-social">
									<ul>
										<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
										<li><a href="#"><i class="fab fa-twitter"></i></a></li>
										<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
										<li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
										<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</header>

				<!-- Navbar -->
				<nav class="navbar navbar-custom">
					<div class="container">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarCollapse" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="index.php">
								<img src="{{ asset('image/logo.png') }}" class="img-responsive" alt="" />
							</a>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div id="navbarCollapse" class="collapse navbar-collapse">
							<ul class="nav navbar-nav">
									<li class="active"><a href="#">HOME</a></li>
									<li><a href="{{url('attorney')}}">MEET OUR ATTORNEYS</a></li>
									<li class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle">PRACTISE AREA<span class="glyphicon glyphicon-chevron-down"></span></a>
									<ul class="dropdown-menu">
										<li><a href="#">Criminal Defense</a></li>
										<li>
											<a href="#" data-toggle="dropdown" class="dropdown-toggle">Drug Crime</a>
											<ul class="dropdown-menu submenu-menu">
												<li><a href="#">Lorem ipsum</a></li>
												<li><a href="#">Lorem ipsum</a></li>
												<li><a href="#">Lorem Ipsum</a></li>
											</ul>
										</li>
										
										<li><a href="#">Cyber Crime</a></li>
										<li><a href="#">Defence</a></li>
										<li><a href="#">Federal Crimes</a></li>
										<li><a href="#">Sex Crimes</a></li>
										<li><a href="{{ url('immigration') }}">Immigration</a></li>
										<li><a href="#">Civil &amp; Litigations</a></li>
										<li><a href="#">Personal Injury</a></li>
										
									</ul>
								</li>
								
								<li><a href="{{url('firm-profile')}}">OUR FIRM PROFILE</a></li>
								<li><a href="#">MEDIA CENTRE</a></li>
								<li><a href="#">TESTIMONIAL</a></li>
								<li><a href="#">CASE RESULT</a></li>
								<li><a href="#">CONTACT US</a></li>        
						</ul>
					</div>
						<!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
				</nav>
        <!--Header ends here-->