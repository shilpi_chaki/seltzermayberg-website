@extends('layouts.app')


@section('content')
	<!-- Slider section -->
        <section>
           <div class="owl-carousel owl-theme" id="home-slider">
						<div class="item">
						 	<div class="home-slider">
						 		<img src="{{ asset('image/banner-image.jpg') }}" class="img-responsive" alt="" />
								<div class="header-banner-gradient"></div>
									<div class="slider-info">
										<div>
											<h2>Injured!<br/> Insurance Company Denied your Claim?</h2>
											<p>It is not about a case, it's about your future.</p>
											<a href="#" class="btn btn-theme btn-white-border margin-in-mobile">Learn More</a>
											<a href="#" class="btn btn-theme btn-white">Free Consultations</a>
										</div>
									</div>
								</div>
						 	</div>
							<div class="item">
						 		<div class="home-slider">
						 		<img src="{{ asset('image/banner-image.jpg') }}" class="img-responsive" alt="" />
								<div class="header-banner-gradient"></div>
									<div class="slider-info">
										<div>
											<h2>Injured!<br/> Insurance Company Denied your Claim?</h2>
											<p>It is not about a case, it's about your future.</p>
											<a href="#" class="btn btn-theme btn-white-border">Learn More</a>
											<a href="#" class="btn btn-theme btn-white">Free Consultations</a>
										</div>
									</div>
								</div>
						 	</div>
							<div class="item">
						 		<div class="home-slider">
						 		<img src="{{ asset('image/banner-image.jpg') }}" class="img-responsive" alt="" />
								<div class="header-banner-gradient"></div>
									<div class="slider-info">
										<div>
											<h2>Injured!<br/> Insurance Company Denied your Claim?</h2>
											<p>It is not about a case, it's about your future.</p>
											<a href="#" class="btn btn-theme btn-white-border">Learn More</a>
											<a href="#" class="btn btn-theme btn-white">Free Consultations</a>
										</div>
									</div>
								</div>
						 	</div>
						</div>
        </section>

				<!-- Clients section -->
				<section class="clients-slider padding-50">
					<div class="container">
						<div class="owl-carousel owl-theme" id="clients-slider-home">
							<div class="item">
								<div class="clients-logo">
									<img src="{{ asset('image/client-01.jpg') }}" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="{{ asset('image/client-02.jpg') }}" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="{{ asset('image/client-03.jpg') }}" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="{{ asset('image/client-04.jpg') }}" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="{{ asset('image/client-05.jpg') }}" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="{{ asset('image/client-06.jpg') }}" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="{{ asset('image/client-07.jpg') }}" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="{{ asset('image/client-01.jpg') }}" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="{{ asset('image/client-02.jpg') }}" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="{{ asset('image/client-03.jpg') }}" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="{{ asset('image/client-04.jpg') }}" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="{{ asset('image/client-05.jpg') }}" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="{{ asset('image/client-06.jpg') }}" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="{{ asset('image/client-07.jpg') }}" class="img-responsive" alt="" />
								</div>
							</div>
						</div>
					</div>
				</section>

			<!-- Welcome Note -->
			<section class="welcome-note padding-75">
				<div class="container">
					<div class="row">
						<div class="col-md-5">
							<img src="{{ asset('image/introduction-image.jpg') }}" class="img-responsive center-block" alt="" />
						</div>
						<div class="col-md-7">
								<h1 class="heading-lighter">Meet our firm</h1>
								<h2 class="heading-main">Why work with Seltzer Mayberg, LLC?</h2>
								<p class="padding-top-10">At Seltzer Mayberg, LLC, we are passionate about fighting for the rights of our clients. We pay close attention to our clients' needs and work to build a strategic defense for their case. With a solid reputation throughout all of Florida, you can be confident in the team you have chosen to advocate for you. Throughout our years in practice, we have achieved many notable verdicts on behalf of our clients.</p>
								<ul class="padding-top-25">
									<li><img src="image/badge-01.png" class="img-responsive" alt="" /></li>
									<li><img src="image/badge-02.png" class="img-responsive" alt="" /></li>
									<li><img src="image/badge-03.png" class="img-responsive" alt="" /></li>
								</ul>
								<a href="#" class="btn btn-theme btn-black margin-top-30">Learn More</a>
						</div>
					</div>
				</div>
			</section>

			<!-- Our Products and Services -->
			<section class="product-services padding-75 text-center">
				<div class="container">
					<div class="heading">
						<h1 class="heading-lighter color-solid">Our Services</h1>
						<h2 class="heading-main color-white padding-bottom-40">Practice Areas</h2>
					</div>
					<div class="products-and-services">
						<div class="row">
							<div class="col-md-6">
								<div class="product-blocks">
									<img src="{{ asset('image/services-01.jpg') }}" class="img-responsive" alt="" />
									<p>P.I Car Accidents</p>
								</div>
							</div>
							<div class="col-md-3">
								<div class="product-blocks">
									<img src="{{ asset('image/services-02.jpg') }}" class="img-responsive" alt="" />
									<p>Criminal Defence State &amp; Federal</p>
								</div>
							</div>
							<div class="col-md-3">
								<div class="product-blocks">
									<img src="{{ asset('image/services-03.jpg') }}" class="img-responsive" alt="" />
									<p>Family Law</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="product-blocks">
									<img src="{{ asset('image/services-04.jpg') }}" class="img-responsive" alt="" />
									<p>Real Estate</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="product-blocks">
									<img src="{{ asset('image/services-05.jpg') }}" class="img-responsive" alt="" />
									<p>Immigration</p>
								</div>
							</div>
							<div class="col-md-3">
								<div class="product-blocks">
									<img src="{{ asset('image/services-06.jpg') }}" class="img-responsive" alt="" />
									<p>bankruptcy workers compensation property claims</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- Effective Solutions -->
			<section class="effective-solutions padding-75">
				<div class="container">
					<div class="row">
						<div class="col-md-8 left-panel">
							<img src="image/man-icon.png" class="img-responsive" alt="" />
							<h3>Not sure what you need? </h3>
							<p>Get a <span>Free Consultation</span> Now</p>
						</div>
						<div class="col-md-4">
							<div class="schedule">
								<img src="image/orange-arrow.png" class="img-responsive" alt="" />
								<a href="#" class="btn btn-schedule">Schedule A Call Now</a>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- Media Center -->
			<section class="featured-works text-center padding-75">
				<div class="container">
					<div class="heading">
						<h1 class="heading-lighter">Media Center</h1>
						<h2 class="heading-main padding-bottom-40">Latest Video</h2>
					</div>
					<div class="row">
						<div class="col-md-5">
							<div class="item featured-blocks">
								<div class="featured-blocks-img" onclick="play();" id="vidwrap" >
									<div class="featured-blocks-img">
										<img src="image/media-center-larger.png" class="img-responsive" alt="" />
									</div>
									<div class="video-icon">
										<i class="far fa-play-circle"></i>
									</div>
									
								</div>
								<div class="featured-blocks-info">
									<h4>Choosing Your Attorney</h4>
									<p>David Seltzer talks about what you need to know when choosing an attorney.</p>
								</div>
							</div>
						</div>
						<div class="col-md-7">
							<div class="row">
								<div class="col-md-6">
									<div class="item featured-blocks">
										<div class="featured-blocks-img">
											<img src="image/media-cente-01.jpg" class="img-responsive" alt="" />
										</div>
										<div class="featured-blocks-info">
											<h4>Choosing Your Attorney</h4>
											<p>David Seltzer talks about what you need to know when choosing an attorney.</p>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="item featured-blocks">
										<div class="featured-blocks-img">
											<img src="image/media-center-02.jpg" class="img-responsive" alt="" />
										</div>
										<div class="featured-blocks-info">
											<h4>Choosing Your Attorney</h4>
											<p>David Seltzer talks about what you need to know when choosing an attorney.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="item featured-blocks">
										<div class="featured-blocks-img">
											<img src="image/media-center-03.jpg" class="img-responsive" alt="" />
										</div>
										<div class="featured-blocks-info">
											<h4>Casey Anthony Trial Opening</h4>
											<p>David Seltzer talks about what you need to know when choosing an attorney.</p>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="item featured-blocks">
										<div class="featured-blocks-img">
											<img src="image/media-center-04.jpg" class="img-responsive" alt="" />
										</div>
										<div class="featured-blocks-info">
											<h4>Fraud Investigation</h4>
											<p>David Seltzer talks about what you need to know when choosing an attorney.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</section>
		
		
		<!-- Our Team-->
		<section class="text-center team-works padding-75">
				<div class="container">
					<div class="heading">
						<h1 class="heading-lighter color-solid">Meet our Attorney</h1>
						<h2 class="heading-main color-white padding-bottom-40">The Team</h2>
					</div>
					<div class="owl-carousel owl-theme" id="team-slider">
							<div class="item team-blocks">
								<div class="team-blocks-img">
									<img src="image/team-01.jpg" class="img-responsive" alt="" />
								</div>
								<div class="team-blocks-info">
									<h4>David Seltzer</h4>
									<p>Criminal lawyer</p>
									<ul>
										<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
										<li><a href="#"><i class="fab fa-twitter"></i></a></li>
										<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
										<li><a href="#"><i class="fab fa-dribbble"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="item team-blocks">
								<div class="team-blocks-img">
									<img src="image/team-02.jpg" class="img-responsive" alt="" />
								</div>
								<div class="team-blocks-info">
									<h4>David Seltzer</h4>
									<p>Criminal lawyer</p>
									<ul>
										<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
										<li><a href="#"><i class="fab fa-twitter"></i></a></li>
										<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
										<li><a href="#"><i class="fab fa-dribbble"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="item team-blocks">
								<div class="team-blocks-img">
									<img src="image/team-03.jpg" class="img-responsive" alt="" />
								</div>
								<div class="team-blocks-info">
									<h4>David Seltzer</h4>
									<p>Criminal lawyer</p>
									<ul>
										<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
										<li><a href="#"><i class="fab fa-twitter"></i></a></li>
										<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
										<li><a href="#"><i class="fab fa-dribbble"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="item team-blocks">
								<div class="team-blocks-img">
									<img src="image/team-01.jpg" class="img-responsive" alt="" />
								</div>
								<div class="team-blocks-info">
									<h4>David Seltzer</h4>
									<p>Criminal lawyer</p>
									<ul>
										<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
										<li><a href="#"><i class="fab fa-twitter"></i></a></li>
										<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
										<li><a href="#"><i class="fab fa-dribbble"></i></a></li>
									</ul>
								</div>
							</div>
							
					</div>

				</div>
			</section>
		
			

			<!-- Clients Section -->
			<section class="padding-75 text-center bg-f6">
				<div class="container">
					<div class="heading">
						<h1 class="heading-lighter">Our Testimonials</h1>
						<h2 class="heading-main padding-bottom-40">What our clients say ?</h2>
					</div>
					<div class="row">
						<div class="col-md-offset-2 col-md-8">
							<div class="owl-carousel owl-theme" id="client-reviews">
								<div class="item">
									<div class="clients-block">
										<img src="image/client-reviews-01.jpg" class="img-responsive center-block" alt="" />
										<p>nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
										<h3>Caspian Bellevedere</h3>
									</div>
								</div>
								<div class="item">
									<div class="clients-block">
										<img src="image/client-reviews-01.jpg" class="img-responsive center-block" alt="" />
										<p>nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
										<h3>Caspian Bellevedere</h3>
									</div>
								</div>
								<div class="item">
									<div class="clients-block">
										<img src="image/client-reviews-01.jpg" class="img-responsive center-block" alt="" />
										<p>nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
										<h3>Caspian Bellevedere</h3>
									</div>
								</div>
								<div class="item">
									<div class="clients-block">
										<img src="image/client-reviews-01.jpg" class="img-responsive center-block" alt="" />
										<p>nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
										<h3>Caspian Bellevedere</h3>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		
			<!-- Case Studies -->
			<section class="welcome-note padding-75 case-studies">
				<div class="container">
					<div class="row">
						<div class="col-md-7">
								<h1 class="heading-lighter">Case Studies</h1>
								<h2 class="heading-main">Our Latest News</h2>
							
								<div class="owl-carousel owl-theme" id="case-studies">
									<div class="item">
										<h3>Broward County State Court – Possession of a Firearm by a Convicted Felon</h3>
										<p class="padding-top-10">Defendant was arrested after discharging a firearm at his apartment complex. Gun was found in proximity to the defendant, with no other individuals in the vicinity. After extensive and lengthy discovery, which included depositions, photos of the scene, and a video of the scene depicting a reenactment of the night in question, Seltzer Mayberg, LLC through its vigorous and persistence was able to secure an amazing result for the defendant.</p>
										<p class="result">Result</p>
										<p>Probation, NO JAIL – got the State to waive the Minimum Mandatory sentence</p>
									</div>
									<div class="item">
										<h3>Broward County State Court – Possession of a Firearm by a Convicted Felon</h3>
										<p class="padding-top-10">Defendant was arrested after discharging a firearm at his apartment complex. Gun was found in proximity to the defendant, with no other individuals in the vicinity. After extensive and lengthy discovery, which included depositions, photos of the scene, and a video of the scene depicting a reenactment of the night in question, Seltzer Mayberg, LLC through its vigorous and persistence was able to secure an amazing result for the defendant.</p>
										<p class="result">Result</p>
										<p>Probation, NO JAIL – got the State to waive the Minimum Mandatory sentence</p>
									</div>
									<div class="item">
										<h3>Broward County State Court – Possession of a Firearm by a Convicted Felon</h3>
										<p class="padding-top-10">Defendant was arrested after discharging a firearm at his apartment complex. Gun was found in proximity to the defendant, with no other individuals in the vicinity. After extensive and lengthy discovery, which included depositions, photos of the scene, and a video of the scene depicting a reenactment of the night in question, Seltzer Mayberg, LLC through its vigorous and persistence was able to secure an amazing result for the defendant.</p>
										<p class="result">Result</p>
										<p>Probation, NO JAIL – got the State to waive the Minimum Mandatory sentence</p>
									</div>
									<div class="item">
										<h3>Broward County State Court – Possession of a Firearm by a Convicted Felon</h3>
										<p class="padding-top-10">Defendant was arrested after discharging a firearm at his apartment complex. Gun was found in proximity to the defendant, with no other individuals in the vicinity. After extensive and lengthy discovery, which included depositions, photos of the scene, and a video of the scene depicting a reenactment of the night in question, Seltzer Mayberg, LLC through its vigorous and persistence was able to secure an amazing result for the defendant.</p>
										<p class="result">Result</p>
										<p>Probation, NO JAIL – got the State to waive the Minimum Mandatory sentence</p>
									</div>
								</div>
								
						</div>
						<div class="col-md-5">
							<img src="image/case-studies.jpg" class="img-responsive center-block" alt="" />
						</div>
					</div>
				</div>
			</section>

			<!-- Effective Solutions -->
			<section class="effective-solutions padding-75">
				<div class="container">
					<div class="row">
						<div class="col-md-8 left-panel">
							<img src="image/man-icon.png" class="img-responsive" alt="" />
							<h3>Not sure what you need? </h3>
							<p>Get a <span>Free Consultation</span> Now</p>
						</div>
						<div class="col-md-4">
							<div class="schedule">
								<img src="image/orange-arrow.png" class="img-responsive" alt="" />
								<a href="#" class="btn btn-schedule">Schedule A Call Now</a>
							</div>
						</div>
					</div>
				</div>
			</section>

@endSection

			
