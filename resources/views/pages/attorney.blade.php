@extends('layouts.app')

@section('content')

<!-- Attorney Heading -->
				<section class="heading-board">
					<img src="image/attorney-banner.jpg" class="img-responsive" alt="" />
					<h1>David Seltzer</h1>
				</section>
		
				<!-- Clients section -->
				<section class="clients-slider padding-50">
					<div class="container">
						<div class="owl-carousel owl-theme" id="clients-slider-home">
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-01.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-02.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-03.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-04.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-05.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-06.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-07.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-01.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-02.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-03.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-04.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-05.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-06.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-07.jpg" class="img-responsive" alt="" />
								</div>
							</div>
						</div>
					</div>
				</section>

				<!-- Welcome Note -->
				<section class="welcome-note padding-75">
					<div class="container">
						<div class="row">
							<div class="col-md-5">
								<img src="image/attoney-01.png" class="img-responsive center-block" alt="" />
							</div>
							<div class="col-md-7">
									<h1 class="heading-lighter">David Seltzer</h1>
									<h2 class="heading-main">About Your Criminal Defense Attorney</h2>
									<p class="padding-top-10">Attorney David Seltzer currently practices criminal defense law in areas throughout South Florida. However, the experienced attorney did not begin here. Born in Canada, David grew up in Montreal, Quebec. He attended Goucher College in Towson, Maryland where he completed his undergraduate studies, majoring in business and minoring in pre-law. Upon graduation in 2001, Mr. Seltzer went directly to work as a paralegal in New York City, assisting on one of the biggest products liability cases in litigation at the time. At the conclusion of his time spent as a paralegal, David then made the transition back to the educational setting, moving to Lansing, Michigan to attend Thomas M. Cooley Law School. After two semesters as an honorable student, ranking among the top ten percent of his class, David again transferred locations, this time settling in Florida to finish up his graduate studies at the University of Miami School of Law.</p>
							</div>
						</div>
						<p class="padding-top-40">During his final years in grad school, David secured an internship at the Miami-Dade State Attorney's Office, where he later became a full-time employee. In fact, Mr. Seltzer spent many of his post-graduate years working here, learning much about cases involving DUI, misdemeanor crimes, juvenile offenses and felony cases. During his years on staff, David played an integral role in substantial legal matters such as assault &amp; battery charges, robbery accusations, drug trafficking arrests, lewd and lascivious molestation of children, weapons cases and so much more. Legal matters of this nature were often handled successfully by the up-and-coming attorney, ultimately earning him tenure as a cyber crimes prosecutor.</p>
						<p>Assigned to the cyber crimes unit, David took his responsibilities seriously, conducting proactive investigations into computer forensics, at the time a newly emerging field of the law. The exposure and training on computer operation and their retention of data gave David an insight into how these cases are prosecuted and defended – knowledge that he uses to this day in the defense he brings to court on behalf of the clients he represents. It was during this time that Mr. Seltzer gained experience in handling cases involving child solicitation, possession of child pornography, unauthorized access to wireless networks, unauthorized access to computer networks, transmission of harmful material to minors and insurance fraud. Playing an integral role in the investigation processes of a number of cases, David also helped draft search warrants and assisted as an invaluable member of the team.</p>
					</div>
				</section>
		
				<section class="padding-75 bg-f6 text-center">
					<div class="container">
						<h2 class="heading-main">Exemplary Education and Experience</h2>
						<p class="font-size-15 padding-top-20">The experience that David gained during the years he spent working for the state attorney's office provided him with knowledge and insight that many seasoned attorneys have yet to experience. Already, Mr. Seltzer has had more training and exposure to internet crimes and how the legal system works than some will have in their entire careers. It was during his time as a prosecutor that David benefited from numerous training sessions and conversations with experts in the field, helping him better understand the complexities of how technology relates to the law.</p>

						<p class="font-size-15 padding-top-20">Both in and out of court, David has proven his creativity and intuition in effectively representing clients. Having tried numerous cases and countless motions, the attorney has proven his ability to win inside the courtroom. His skills extend outside the halls of justice as well, where he actively negotiates settlements on behalf of his clients whose cases do not need to go to court. Numerous legal matters have been successfully resolved by the attorney who has become an experienced litigator both in and out of the courtroom.</p>
					</div>
				</section>
		
				<!-- Education Background -->
				<section class="attorney-education">
					<img src="image/attorney-02.jpg" class="img-responsive" alt="" />
					<div class="education-overlay">
						<div class="padding-75">
							<div class="container">
								<div class="row">
									<div class="col-md-5">
										<div class="education-attorney-panel">
											<div>
												<h2 class="heading-main white padding-bottom-20">Education Background</h2>
												<ul class="upper">
													<li><i class="fas fa-check-circle"></i>Juris Doctorate, University of Miami: 2004</li>
													<li><i class="fas fa-check-circle"></i>Bachelor of Arts, Goucher College: 2001</li>
													<li><i class="fas fa-check-circle"></i>Demystifying Cyber Crime, Lecturer, Advanced Judicial Studies, Florida – 2010 &amp; 2012</li>
												</ul>
											</div>
											<div>
												<h2 class="heading-main white padding-bottom-20 padding-top-20">Professional and Bar Association Memberships</h2>
												<ul>
													<li><i class="fas fa-check-circle"></i>CABA Member, 2008</li>
													<li><i class="fas fa-check-circle"></i>FACDL Member since 2008</li>
													<li><i class="fas fa-check-circle"></i>NACDL Member since 2008</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="col-md-offset-1 col-md-5">
										<div class="education-attorney-panel">
											<div>
												<h2 class="heading-main white padding-bottom-20">Admitted to Practice in the Following Jurisdictions</h2>
												<ul class="upper">
													<li><i class="fas fa-check-circle"></i>Southern District of Florida: 2007</li>
													<li><i class="fas fa-check-circle"></i>Middle District of Florida: 2007</li>
													<li><i class="fas fa-check-circle"></i>Northern District of Florida: 2007</li>
													<li><i class="fas fa-check-circle"></i>District of Columbia: 2006</li>
													<li><i class="fas fa-check-circle"></i>Florida: 2004</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

@endsection