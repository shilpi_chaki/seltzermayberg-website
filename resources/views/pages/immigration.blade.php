@extends('layouts.app')

@section('content')

<!-- Immigration Heading -->
				<section class="heading-board">
					<img src="image/immigration-breadcrumbs.jpg" class="img-responsive" alt="" />
					<h1>Immigration</h1>
				</section>
		
				<!-- Clients section -->
				<section class="clients-slider padding-50">
					<div class="container">
						<div class="owl-carousel owl-theme" id="clients-slider-home">
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-01.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-02.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-03.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-04.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-05.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-06.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-07.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-01.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-02.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-03.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-04.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-05.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-06.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-07.jpg" class="img-responsive" alt="" />
								</div>
							</div>
						</div>
					</div>
				</section>

			<!-- Welcome Note -->
			<section class="welcome-note padding-75">
				<div class="container">
					<div class="row">
						<div class="col-md-5">
							<img src="image/immigration-01.png" class="img-responsive center-block" alt="" />
						</div>
						<div class="col-md-7">
								<h1 class="heading-lighter">Immigration</h1>
								<h2 class="heading-main">Immigration and Criminal Charges in Miami</h2>
								<p class="padding-top-10">Obtaining a visa, permanent resident card (green card) or naturalization can sometimes be a very complicated process. If you have also been charged with a crime, it can be almost impossible. The government has very strict procedures regarding the filing of petitions for visas, green cards and naturalization that are at times very confusing, even to most attorneys who do not practice immigration law. At Seltzer Mayberg, LLC, we are aggressive and have a vast amount of experience and knowledge when it comes to immigration law and criminal defense law. In this era of immigration reform, the government has become increasingly vigilant in analyzing all petitions which are filed and in many cases, they deny petitions which should be granted in their over zealousness, and are not showing leniency on criminally convicted immigrants.</p>
						</div>
					</div>
				</div>
			</section>
		
			<!-- Effective Solutions -->
			<section class="effective-solutions padding-75">
				<div class="container">
					<div class="row">
						<div class="col-md-8 left-panel">
							<img src="image/man-icon.png" class="img-responsive" alt="" />
							<h3>Not sure what you need? </h3>
							<p>Get a <span>Free Consultation</span> Now</p>
						</div>
						<div class="col-md-4">
							<div class="schedule">
								<img src="image/orange-arrow.png" class="img-responsive" alt="" />
								<a href="#" class="btn btn-schedule">Schedule A Call Now</a>
							</div>
						</div>
					</div>
				</div>
			</section>
		
		<!-- Immigration Information -->
		<section class="padding-75">
			<div class="container">
				<h2 class="heading-main padding-bottom-50 text-center">Deportation Defense: Criminal Attorney in Miami</h2>
				<div class="row padding-bottom-50">
					<div class="col-md-4">
						<img src="image/immigration-02.jpg" class="img-responsive" alt="" />
					</div>
					<div class="col-md-4">
						<img src="image/immigration-03.jpg" class="img-responsive margin-top-bottom-10-mobile" alt="" />
					</div>
					<div class="col-md-4">
						<img src="image/immigration-04.jpg" class="img-responsive" alt="" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-offset-1 col-md-10">
						<p class="font-size-15">An experienced and knowledgeable immigration attorney can ease the process of dealing with the United States Department of Homeland Security. If you have been arrested and are facing criminal charges, you need an experienced criminal defense attorney who is intimately familiar with immigration law as well. By having in-depth knowledge about the procedures and the laws and regulations dealing with immigration, an experienced immigration law attorney can see the pitfalls and possible outcomes of a filing before the government can stall the process. There may be several ways which an immigrant can apply for the right to live and/or work in the US. We can find the best option for you to attain your goal, whether it be a non-immigrant work visa, a business visa, an investor's visa or a family petition based on marriage or some other familial connection.</p>
						<p class="font-size-15">If you have already filed a petition and have since been arrested, this puts your current status in serious jeopardy. If you find yourself the subject of deportation proceedings, removal or federal prosecution for immigration related fraud, do not attempt to handle these matters on your own without an immigration attorney. We can help you navigate the murky waters of immigration law. With the laws changing from year to year, you need someone who has the legal sophistication, successful track record and unwavering dedication to immigration law in your corner. Call our offices at Seltzer Mayberg, LLC today to schedule a free evaluation of your circumstances.</p>
					</div>
				</div>
				
			</div>
		</section>
		
		<!-- Clients Section -->
		<section class="padding-75 text-center bg-f6">
			<div class="container">
				<div class="heading">
					<h1 class="heading-lighter">Our Testimonials</h1>
					<h2 class="heading-main padding-bottom-40">What our clients say ?</h2>
				</div>
				<div class="row">
					<div class="col-md-offset-2 col-md-8">
						<div class="owl-carousel owl-theme" id="client-reviews">
							<div class="item">
								<div class="clients-block">
									<img src="image/client-reviews-01.jpg" class="img-responsive center-block" alt="" />
									<p>nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
									<h3>Caspian Bellevedere</h3>
								</div>
							</div>
							<div class="item">
								<div class="clients-block">
									<img src="image/client-reviews-01.jpg" class="img-responsive center-block" alt="" />
									<p>nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
									<h3>Caspian Bellevedere</h3>
								</div>
							</div>
							<div class="item">
								<div class="clients-block">
									<img src="image/client-reviews-01.jpg" class="img-responsive center-block" alt="" />
									<p>nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
									<h3>Caspian Bellevedere</h3>
								</div>
							</div>
							<div class="item">
								<div class="clients-block">
									<img src="image/client-reviews-01.jpg" class="img-responsive center-block" alt="" />
									<p>nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
									<h3>Caspian Bellevedere</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<!-- Effective Solutions -->
			<section class="effective-solutions padding-75">
				<div class="container">
					<div class="row">
						<div class="col-md-8 left-panel">
							<img src="image/man-icon.png" class="img-responsive" alt="" />
							<h3>Not sure what you need? </h3>
							<p>Get a <span>Free Consultation</span> Now</p>
						</div>
						<div class="col-md-4">
							<div class="schedule">
								<img src="image/orange-arrow.png" class="img-responsive" alt="" />
								<a href="#" class="btn btn-schedule">Schedule A Call Now</a>
							</div>
						</div>
					</div>
				</div>
			</section>

@endsection