@extends('layouts.app')

@section('content')

<!-- Firm Profile Heading -->
				<section class="heading-board">
					<img src="image/firm-profile-banner.jpg" class="img-responsive" alt="" />
					<h1>Our Firm Profile</h1>
				</section>
		
				<!-- Clients section -->
				<section class="clients-slider padding-50">
					<div class="container">
						<div class="owl-carousel owl-theme" id="clients-slider-home">
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-01.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-02.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-03.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-04.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-05.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-06.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-07.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-01.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-02.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-03.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-04.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-05.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-06.jpg" class="img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="clients-logo">
									<img src="image/client-07.jpg" class="img-responsive" alt="" />
								</div>
							</div>
						</div>
					</div>
				</section>

			<!-- Welcome Note -->
			<section class="welcome-note padding-75">
				<div class="container">
					<div class="row">
						<div class="col-md-5">
							<img src="image/firm-profile-01.png" class="img-responsive center-block" alt="" />
						</div>
						<div class="col-md-7">
								<h1 class="heading-lighter">Meet our firm</h1>
								<h2 class="heading-main">Why work with Seltzer Mayberg, LLC?</h2>
								<p class="padding-top-10">At Seltzer Mayberg, LLC, we are passionate about fighting for the rights of our clients. We pay close attention to our clients' needs and work to build a strategic defense for their case. With a solid reputation throughout all of Florida, you can be confident in the team you have chosen to advocate for you. Throughout our years in practice, we have achieved many notable verdicts on behalf of our clients.</p>
								<h2 class="heading-main">We have also received the following honors</h2>
								<ul class="padding-top-25">
									<li><img src="image/badge-01.png" class="img-responsive" alt="" /></li>
									<li><img src="image/badge-02.png" class="img-responsive" alt="" /></li>
									<li><img src="image/badge-03.png" class="img-responsive" alt="" /></li>
								</ul>
						</div>
					</div>
				</div>
			</section>
		
			<section class="attorney-education firm-profile-height">
					<img src="image/firm-profile-02.jpg" class="img-responsive" alt="" />
					<div class="education-overlay">
						<div class="padding-75">
							<div class="container">
								<div class="row">
									<div class="col-md-6">
										<div class="education-attorney-panel">
											<div>
												<p class="white padding-bottom-10"><strong>Unlike other law firms, we do not use an answering service. </strong></p>
												<p class="white padding-bottom-10">Our Miami criminal defense lawyer is available to speak with clients <strong>24 HOURS A DAY, 7 DAYS A WEEK.</strong></p>
												<p class="white padding-bottom-10">We will take the time to address 
													<strong>Your case with the following characteristics :</strong></p>
											</div>
										</div>
									</div>
									<div class="col-md-offset-1 col-md-5">
										<div class="education-attorney-panel">
											<div>
												<ul class="upper">
													<li><i class="fas fa-check-circle"></i>Personalized Focus</li>
													<li><i class="fas fa-check-circle"></i>Commitment</li>
													<li><i class="fas fa-check-circle"></i>Aggressive Defence</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
		
				<!-- Immigration Information -->
				<section class="padding-75">
					<div class="container">
						<h2 class="heading-main padding-bottom-50 text-center">Handling the toughest criminal charges</h2>
						<div class="row padding-bottom-50">
							<div class="col-md-4">
								<img src="image/firm-profile-03.jpg" class="img-responsive" alt="" />
							</div>
							<div class="col-md-4">
								<img src="image/firm-profile-04.jpg" class="img-responsive margin-top-bottom-10-mobile" alt="" />
							</div>
							<div class="col-md-4">
								<img src="image/firm-profile-05.jpg" class="img-responsive" alt="" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-offset-1 col-md-10">
								<p class="font-size-15 text-center">When you choose to work with our firm, you will be provided with continual support and protection under the law throughout each stage of your case. Regardless of what you were charged with, you can have the confidence that our team is working non-stop to identify the right defense strategy for your negotiations and for your trial.</p>
							</div>
						</div>

					</div>
				</section>
		
				<!-- Effective Solutions -->
				<section class="effective-solutions padding-75">
					<div class="container">
						<div class="row">
							<div class="col-md-8 left-panel">
								<img src="image/man-icon.png" class="img-responsive" alt="" />
								<h3>Not sure what you need? </h3>
								<p>Get a <span>Free Consultation</span> Now</p>
							</div>
							<div class="col-md-4">
								<div class="schedule">
									<img src="image/orange-arrow.png" class="img-responsive" alt="" />
									<a href="#" class="btn btn-schedule">Schedule A Call Now</a>
								</div>
							</div>
						</div>
					</div>
				</section>
		
				<!-- Welcome Note -->
				<section class="welcome-note padding-75">
					<div class="container">
						<div class="row">
							<div class="col-md-7">
									<h1 class="heading-lighter">our services</h1>
									<h2 class="heading-main padding-top-10">We provide high-quality defense for the following crimes:</h2>
									<ul class="firm-profile-check">
										<li><i class="fas fa-check-circle"></i>Drug Charges</li>
										<li><i class="fas fa-check-circle"></i>Fraud Crimes</li>
										<li><i class="fas fa-check-circle"></i>Theft Crimes</li>
										<li><i class="fas fa-check-circle"></i>Computer Crimes</li>
										<li><i class="fas fa-check-circle"></i>Juvenile Crimes</li>
										<li><i class="fas fa-check-circle"></i>Sex Crimes</li>
										<li><i class="fas fa-check-circle"></i>DUI Charges</li>
										<li><i class="fas fa-check-circle"></i>Federal Crimes</li>
									</ul>
									<p class="padding-top-10">ou should make sure you are working with a professional firm to get the tough criminal defense you deserve. If you choose to work with Seltzer Mayberg, LLC, your case will be handled with the level of attention and zealousness it deserves. We take proactive steps to address issues that may arise throughout your case. Our firm can carefully prepare your case to defend against the tactics of prosecution.</p>
							</div>
							<div class="col-md-5">
								<img src="image/firm-profile-05.png" class="img-responsive center-block" alt="" />
							</div>
						</div>
					</div>
				</section>
		
				<!-- Workings -->
				<section class="firm-profile-details">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6">
								<div class="left-firm-section">
									<img src="image/firm-profile-06.jpg" class="img-responsive center-block" alt="" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="right-firm-section">
									<p>Our founding attorney, David S. Seltzer, and the entire legal team is committed to litigating cases on behalf of our clients. We focus all of our firm's efforts on adequately protecting the rights of our clients. Call us [6] today for the legal protection you need if you are under investigation in a criminal case or are faced with state or federal criminal charges.</p>
								</div>
							</div>
						</div>
					</div>
				</section>
		
				
		
				<!-- Clients Section -->
				<section class="padding-75 text-center bg-f6">
					<div class="container">
						<div class="heading">
							<h1 class="heading-lighter">Our Testimonials</h1>
							<h2 class="heading-main padding-bottom-40">What our clients say ?</h2>
						</div>
						<div class="row">
							<div class="col-md-offset-2 col-md-8">
								<div class="owl-carousel owl-theme" id="client-reviews">
									<div class="item">
										<div class="clients-block">
											<img src="image/client-reviews-01.jpg" class="img-responsive center-block" alt="" />
											<p>nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
											<h3>Caspian Bellevedere</h3>
										</div>
									</div>
									<div class="item">
										<div class="clients-block">
											<img src="image/client-reviews-01.jpg" class="img-responsive center-block" alt="" />
											<p>nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
											<h3>Caspian Bellevedere</h3>
										</div>
									</div>
									<div class="item">
										<div class="clients-block">
											<img src="image/client-reviews-01.jpg" class="img-responsive center-block" alt="" />
											<p>nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
											<h3>Caspian Bellevedere</h3>
										</div>
									</div>
									<div class="item">
										<div class="clients-block">
											<img src="image/client-reviews-01.jpg" class="img-responsive center-block" alt="" />
											<p>nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
											<h3>Caspian Bellevedere</h3>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
		
				<!-- Effective Solutions -->
				<section class="effective-solutions padding-75">
					<div class="container">
						<div class="row">
							<div class="col-md-8 left-panel">
								<img src="image/man-icon.png" class="img-responsive" alt="" />
								<h3>Not sure what you need? </h3>
								<p>Get a <span>Free Consultation</span> Now</p>
							</div>
							<div class="col-md-4">
								<div class="schedule">
									<img src="image/orange-arrow.png" class="img-responsive" alt="" />
									<a href="#" class="btn btn-schedule">Schedule A Call Now</a>
								</div>
							</div>
						</div>
					</div>
				</section>

@endsection